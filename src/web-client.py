#!/usr/bin/env python
from threading import Thread
import SimpleHTTPServer
import sys
import json
import logging
import os
import subprocess
from simple_websocket_server import WebSocketServer, WebSocket # pip install simple_websocket_server


import rospy
import roslaunch
import rospkg
from actionlib import SimpleActionClient
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from nav_msgs.msg import OccupancyGrid, MapMetaData, Path
from std_msgs.msg import String, Int8
from geometry_msgs.msg import Point, PoseStamped
from rosflight_msgs.msg import Status, RCRaw, Command
from rosflight_msgs.srv import ParamGet, ParamSet
from sensor_msgs.msg import Range
from gazebo_msgs.msg import ModelState
# # # # # WEBSOCKET SERVER # # # # #

clients = []
 

class WSHandler(WebSocket):
    def handle(self):
        # handle message --> self.data
        obj = json.loads(self.data)
        if obj["type"] == "reset_sim":    
            # reset simulation data         
            state_msg = ModelState()
            state_msg.model_name = 'multirotor'
            state_msg.pose.position.x = 0
            state_msg.pose.position.y = 0
            state_msg.pose.position.z = 0.3
            state_msg.pose.orientation.x = 0
            state_msg.pose.orientation.y = 0
            state_msg.pose.orientation.z = 0
            state_msg.pose.orientation.w = 1
            rospy.Publisher("gazebo/set_model_state", ModelState, queue_size=10).publish(state_msg)
            # reset hector map
            rospy.Publisher("syscommand", String, queue_size=10).publish("reset")
            # clear costmap
            os.system("rosservice call /move_base/clear_costmaps")
        elif obj["type"] == "target":
            p = Point()
            p.x = obj["data"]["x"]
            p.y = obj["data"]["y"]
            actionClient = SimpleActionClient("move_base", MoveBaseAction)  
            actionClient.wait_for_server()
            goal = MoveBaseGoal()
            goal.target_pose.header.stamp = rospy.Time().now()
            goal.target_pose.header.frame_id = "map"
            goal.target_pose.pose.position.x = p.x
            goal.target_pose.pose.position.y = p.y
            goal.target_pose.pose.orientation.w = 1.0
            actionClient.send_goal(goal)
        elif obj["type"] == "take_off":
            rospy.Publisher("main/control", Int8, queue_size=10).publish(1)
        elif obj["type"] == "landing":
            rospy.Publisher("main/control", Int8, queue_size=10).publish(2)
        elif obj["type"] == "get_map":
            t = Thread(target=sendCroppedMap, args=())
            t.start()
          
    def connected(self):
        print(self.address, 'connected')
        clients.append(self)

    def handle_close(self):
        clients.remove(self)
        print(self.address, 'closed')


server = WebSocketServer('', 8000, WSHandler)

def startWSServer():
    server.serve_forever()

def sendToAll(data):
     for client in clients:
            client.send_message(data.decode('utf-8'))


# # # # # PROCESS HANDLING # # # # #
def processRunning(name):
    proc1 = subprocess.Popen(['ps', 'aux'], stdout=subprocess.PIPE)
    proc2 = subprocess.Popen(['grep', name], stdin=proc1.stdout,
                                        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    proc1.stdout.close() 
    out, err = proc2.communicate()
    lines = out.split("\n")
    return len(lines) > 2

def getProcessStates():
    obj = dict()
    obj["rosflight_sim"] = processRunning("ams-simu main.launch")
    obj["rc_keyboard"] = processRunning("rc_keyboard")
    return obj
   

# # # # # MAP Preparation # # # # #

map = OccupancyGrid()

def sendCroppedMap():
    sendToAll(json.dumps(getCroppedMap()))

def getCroppedMap():
    global map
    obj = dict()
    obj["type"] = "map"
    obj["data"] = dict()
    
    # find map bounds
    left = map.info.width
    right = 0
    top = map.info.height
    bottom = 0
    y = 0
    while y < map.info.height:
        x = 0
        while x < map.info.width: 
            index = y*map.info.width+x
            if not map.data[index] == -1:
                if x < left:
                    left = x
                if x > right:
                    right = x
                if y < top:
                    top = y
                if y > bottom:
                    bottom = y 
            x += 1
        y += 1
    # prepare data
    obj["data"]["height"] = bottom-top+1
    obj["data"]["width"] = right-left+1
    obj["data"]["resolution"] = map.info.resolution
    obj["data"]["originX"] = (map.info.origin.position.x /map.info.resolution)+left
    obj["data"]["originY"] = (map.info.origin.position.y /map.info.resolution)+top
    obj["data"]["points"] = []
    y = top
    while y <= bottom:
        x = left
        while x <= right: 
            index = y*map.info.width+x
            obj["data"]["points"].append(map.data[index])
            x += 1
        y += 1
    return obj

# # # # # ROS NODE # # # # #

def statusSubscription(data):
    obj = dict()
    obj["type"] = "status"
    obj["data"] = dict()
    obj["data"]["armed"] = data.armed
    obj["data"]["failsafe"] = data.failsafe
    obj["data"]["rc_override"] = data.rc_override
    obj["data"]["offboard"] = data.offboard
    obj["data"]["control_mode"] = data.control_mode
    sendToAll(json.dumps(obj))

def rcRawSubscription(data):
    obj = dict()
    obj["type"] = "rc_raw"
    obj["data"] = dict()
    obj["data"]["x"] = data.values[0] 
    obj["data"]["y"] = data.values[1] 
    obj["data"]["z"] = data.values[2] 
    obj["data"]["F"] = data.values[3] 
    obj["data"]["aux1"] = data.values[4]
    obj["data"]["aux2"] = data.values[5]
    obj["data"]["aux3"] = data.values[6]
    obj["data"]["aux4"] = data.values[7]
    sendToAll(json.dumps(obj))

def commandSubscription(data):
    obj = dict()
    obj["type"] = "command"
    obj["data"] = dict()
    obj["data"]["x"] = data.x
    obj["data"]["y"] = data.y
    obj["data"]["z"] = data.z
    obj["data"]["F"] = data.F
    sendToAll(json.dumps(obj))

simParamsSet = False

def setSimParams():
    os.system("rosservice call /param_set ARM_CHANNEL 4 && rosservice call /param_set MIXER 2 && rosservice call /calibrate_imu")
    os.system("rosservice call /param_set RC_THR_OVRD_CHN 7 && rosservice call /param_set RC_ATT_OVRD_CHN 7 && rosservice call /param_set SWITCH_8_DIR 0 && rosservice call /param_set MIN_THROTTLE 0 && rosservice call /param_set SWITCH_8_DIR 1")
       
def mapSubscription(data):
    global map, simParamsSet
    
    if not simParamsSet: # initialize simulation parameters
        t = Thread(target=setSimParams, args=())
        t.start()
        simParamsSet = True

    map = data

last_pose_time = 0
def poseSubscription(data):
    global last_pose_time, is_flying
    now = rospy.get_time()
    if now-last_pose_time > 0.1:
        obj = dict()
        obj["type"] = "pose"
        obj["data"] = dict()
        obj["data"]["x"] = data.pose.position.x
        obj["data"]["y"] = data.pose.position.y
        obj["data"]["is_flying"] = is_flying
        sendToAll(json.dumps(obj))
        last_pose_time = now

def pathSubscription(data):
    path = Path()
    path = data
    targetList = []
    targetIndex = 0
    for pose in path.poses:
        targetList.append([pose.pose.position.x,pose.pose.position.y])
    obj = dict()
    obj["type"] = "path"
    obj["data"] = dict()
    obj["data"]["points"] = targetList;
    sendToAll(json.dumps(obj))

is_flying = False
def sonarSubscription(data):
    global is_flying
    if data.range > 0.25:
        is_flying = True
    else: 
        is_flying = False

def ws_monitor():
    rospy.init_node("ws_monitor", anonymous=True)
    rospy.Subscriber("status", Status, statusSubscription)   
    rospy.Subscriber("rc_raw", RCRaw, rcRawSubscription)    
    rospy.Subscriber("sonar", Range, sonarSubscription)
    rospy.Subscriber("command", Command, commandSubscription)   
    rospy.Subscriber("map", OccupancyGrid, mapSubscription)   
    rospy.Subscriber("pose_est", PoseStamped, poseSubscription) 
    rospy.Subscriber("/move_base/TrajectoryPlannerROS/global_plan", Path, pathSubscription)


# # # # # HTTP Server # # # # # 
def startHTTPServer():
    originDir = os.getcwd() # get current dir
    rospack = rospkg.RosPack()
    packageDir = rospack.get_path('ams-simu')
    os.chdir(packageDir + "/html") # change to html dir
    proc1 = subprocess.Popen(['python', '-m', 'SimpleHTTPServer', '9000'], stdout=subprocess.PIPE)
    proc1.stdout.close() 
    os.chdir(originDir) # back to previous dir


# # # # # MAIN # # # # # 

if __name__ == '__main__':
    try:
        startHTTPServer() 
        ws_monitor()
        startWSServer()
    except rospy.ROSInterruptException:
        rospy.loginfo("exception")
