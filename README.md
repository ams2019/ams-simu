# AMS Simulation

This repository contains all code used for simulation of the drone with gazebo.

## Requirements

1. Install correct ROS version for the system

- for ubuntu 16.04 (xenial) use [kinetic](http://wiki.ros.org/kinetic/Installation/Ubuntu)
- for ubuntu 18.04 (bionic) use [melodic](http://wiki.ros.org/melodic/Installation/Ubuntu)

1. Setup catkin workspace in `~/catkin_ws`: [Creating a workspace for catkin](https://wiki.ros.org/catkin/Tutorials/create_a_workspace)

2. Setup rosflight simulation (cf. [Running Simulations in Gazebo](https://docs.rosflight.org/user-guide/gazebo_simulation/))

        cd ~/catkin_ws/src
        // add rosflight
        git clone https://github.com/rosflight/rosflight.git
        cd rosflight
        git submodule update --init --recursive
        cd ..
        // add rosflight_joy
        git clone https://github.com/rosflight/rosflight_joy.git
        pip install --user pygame
        // add eigen_stl_containers
        git clone https://github.com/ros/eigen_stl_containers
        cd ..
        // build catkin workspace
        catkin_make

3. Install velodyne laser

        sudo apt install ros-"$(rosversion -d)"-velodyne
        sudo apt install ros-"$(rosversion -d)"-velodyne-description

4. Install hector slam packages

        sudo apt install ros-"$(rosversion -d)"-hector-mapping
        sudo apt install ros-"$(rosversion -d)"-hector-imu-attitude-to-tf

5. Install ROS navigation packages

        sudo apt install ros-"$(rosversion -d)"-navigation

6. Install simple_websocket_server

        pip install simple_websocket_server

7. Symlink/copy `ams-main` repository inside `~/catkin_ws/src` (for installation see ams-main/README.md)

## Installation

Clone `ams-simu` into home directory

    cd ~
    git clone https://Rafael91@bitbucket.org/ams2019/ams-simu.git

Symlink `ams-simu` folder into catkin workspace

    sudo ln -s ~/ams-simu ~/catkin_ws/src/ams-simu

Enter catkin workspace and build

    cd ~/catkin_ws
    catkin_make

## How to use

Large symmetric room:

    roslaunch ams-simu room1.launch

Asymmetric room:

    roslaunch ams-simu room2.launch

Multiple rooms:

    roslaunch ams-simu room3.launch
